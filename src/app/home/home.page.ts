import { Component, OnInit } from '@angular/core';
import { StoreService } from '../store.service';
import { Observable } from 'rxjs';
import { StoreListService } from '../store-list.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  store$: Observable<any>;

  constructor(
    public storeService: StoreService
  ) {
  }

  ngOnInit() {
    this.store$ = this.storeService.createStore();
  }
}

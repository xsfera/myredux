import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { startWith, scan } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  initialState = {
    counter: 0,
    list: []
  };

  subject$: ReplaySubject<any>;

  handlers = {
    INCREMENT: state => ({ ...state, counter: state.counter + 1 }),
    DECREMENT: state => ({ ...state, counter: state.counter - 1 }),
    ADD: (state, action) => ({ ...state, counter: state.counter + action.payload }),

    ADDLIST: (state) => ({ ...state, list: state.list.concat(state.list.length + 1) }),

    DEFAULT: state => state
  };

  constructor() {
    this.subject$ = new ReplaySubject(1);
  }

  reducer = (state = this.initialState, action?) => {
    const handler = this.handlers[action.type] || this.handlers.DEFAULT;
    return handler(state, action);
  }

  createStore(rootReducer = this.reducer) {
    return this.subject$.asObservable()
      .pipe(
        startWith({ type: 'INIT' }),
        scan(rootReducer, undefined),
      );
  }

  dispatch(action) {
    this.subject$.next(action);
  }

}
